# Por motivos de permisos, hay que dar todos los permisos a todos los usuarios para evitar problemas.
sudo chmod 777 ./public/uploads


# Creamos el proyecto con:
symfony new symfony_tutorial

# Instalamos annotations
composer require annotations
# Esto nos sirve para pasar de esto:
#    /**
#     *
#     *
#     * @Route("/old", name="old")
#     */
# A esto:
# #[Route('/movies', name: 'app_movies')]

# Instalamos doctrine y maker
composer require doctrine maker

# Creamos un controlador MoviesController
symfony console make:controller MoviesController

# Create an annotations.yaml in config/routes/annotations.yaml and put the follow:
controllers:
    resource: ../../src/Controller/
    type: annotation

kernel:
    resource: ../../src/Kernel.php
    type: annotation

# View the routes with the command
symfony console debug:router


# Instalar el motor de vistas twig
composer require twig

# Instalar orm-pack
composer require symfony/orm-pack

# Instalar maker-bundle
composer require --dev symfony/maker-bundle
no
yes

# Configurar variable de entorno en .env
DATABASE_URL="mysql://root:password@mariadb:3306/movies?serverVersion=5.7&charset=utf8mb4"

# Crear la base de datos con doctrine
symfony console doctrine:database:create

# Crear una entidad de clase
symfony console make:entity
Movie
title
string
255
no
ctrl+c para salir

# Crear un actor
symfony console make:entity Actor

# Crear las tablas (hacer migración) en un archivo antes de crear las tablas. (Para verificar que todo este bien)
symfony console make:migration

# Crear las tablas en la base de datos
symfony console doctrine:migrations:migrate


# Data fixtures
composer require --dev doctrine/doctrine-fixtures-bundle

# I dont know
symfony console doctrine:fixtures:load

# crear archivo MovieFixtures.php con una copia de AppFixtures de src/DataFixtures/
# creamos una nueva clase de Movie y añadimos los campos necesarios, ver apendice.
# y volvermos a poner el comando:
symfony console doctrine:fixtures:load


# Por si se requiere empaquetar nuestra app. Usamos webpack
# https://www.youtube.com/watch?v=QPky3r2prEI
# 02:14:24

# Instalar librería de composer para empaquetar
composer require symfony/webpack-encore-bundle

# Cada que modifiques los archivos assets tienes que poner
npm run dev

# Para manejar los assets en nuestro proyecto instalamos:
composer require symfony/asset

# y en las plantillas incluimos lo siguiente:
<link rel="stylesheet" href="{{ asset('build/app.css') }}">

#Opcional instalar tailwind con npm
npm install -D tailwindcss postcss-loader purgecss-webpack-plugin glob-all path

# Init tailwindcss
npx tailwind init -p

# Haces una configuraciones y pones esto
npx tailwindcss -i ./assets/styles/app.css -o ./public/build/app.css --watch


# Install loader in NPM for images assets
npm install file-loader --save-dev

# Modificar el webpack.config.js
  .copyFiles({
    from: './assets/images',
    to: './images/[path][name].[hash:8].[ext]',
    pattern: /\.(png|jpg|jpeg)$/
  })

# Correr el npm run dev para compilar los assets
npm run dev
# Si salen errores no importa


# Instalar symfony/form para manejar mas facil los formularios
composer require symfony/form

# Creamos un formulario para nuestra entidad Movie
symfony console make:form MovieFormType Movie

# Instalamos symfony/mime ya que es necesario para obtener la extension de un archivo en un formulario.
composer require symfony/mime

#Para validar campos en la base de datos debemos instalar los siguiente:
composer require symfony/validator doctrine/annotations


# Para implementar un sistema de login y register necesitamos de (para autentificación y autorización)
composer require symfony/security-bundle


# Crear un nuevo usuario en nuestra aplicación:
symfony console make:user User

# Hacer migración
symfony console make:migration
symfony console doctrine:migrations:migrate

# Crear un formulario de registrar con 1 simple comando
symfony console make:registration-form
yes
no
yes

# Crear un formulario para login con 1 simple comando:
symfony console make:auth
1
LoginFormAuthenticator
SecurityController
yes








############## APENDICES ##############33

# Ejemplo de una ruta con parametros, verbos y parametros por default

    #[Route('/movies/{name}', name: 'movies', defaults: ['name' => null], methods: ['GET', 'HEAD'])]
    public function index($name): JsonResponse
    {
        return $this->json([
            'message' => $name,
            'path' => 'src/Controller/MoviesController.php',
        ]);
    }

# Ejemplo de una clase Fixtures

class MovieFixtures extends Fixture
{
  public function load(ObjectManager $manager): void
  {
    $movie = new Movie();
    $movie->setTitle('The Dark Knight');
    $movie->setReleaseYear(2008);
    $movie->setDescription('This is the description of the Dark Knight');
    $movie->setImagePath('https://cdn.pixabay.com/photo/2021/06/18/11/22/batman-6345897_960_720.jpg');
    $manager->persist($movie);

    $movie2 = new Movie();
    $movie2->setTitle('Avengers: Endgame');
    $movie2->setReleaseYear(2019);
    $movie2->setDescription('This is the description of the Avengers: Endgame');
    $movie2->setImagePath('https://cdn.pixabay.com/photo/2020/10/28/10/02/captain-america-5692937_960_720.jpg');
    $manager->persist($movie2);

    $manager->flush();
  }
}




// findAll(); - SELECT * FROM movies;
// find(5); - SELECT * FROM movies WHERE id = 5;
// findBy([], ['id' => 'DESC']); - SELECT * FROM movies ORDER BY id DESC
// findOneBy(['id' => 5, 'title' => 'The Dark Knight'], ['id' => 'DESC']); - SELECT * FROM movies WHERE id = 6 AND title = 'The Dark Knight' ORDER BY id DESC
//count(['id' => 5]); - SELECT COUNT() from movies WHERE id = 1



private $em;

public function __construct(EntityManagerInterface $em)
{
  $this->em = $em;
}
